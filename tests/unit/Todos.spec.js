import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import Todos from '@/components/Todos.vue'

describe('Todos.vue', () => {
    let wrapper
    beforeEach(()=>{
         wrapper = mount(Todos)
      })
    it('should be init',()=>{
      expect(wrapper.text()).to.include("All Todos")
      expect(wrapper.find('.test-div').exists()).to.equal(true)
      expect(wrapper.find('.test-input').exists()).to.equal(true)
      expect(wrapper.find('.btn-success').exists()).to.equal(true)
    })
    it('should return all todo', () => {
      wrapper.setData({todoList:[{_id:123,Content:"test1",Completed:false},{_id:124,Content:"test2",Completed:false}]})
      expect(wrapper.vm.todoList.length).to.equal(2)
      expect(wrapper.vm.todoList[0]._id).to.equal(123)
    })
    it('should add a todo',()=>{
      var button=wrapper.find('.btn-success')
      var input=wrapper.find('.test-input')
      input.setValue("hi todo test")
      button.trigger('click')
    })
})
