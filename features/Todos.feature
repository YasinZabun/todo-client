Feature: Todos behavior

Scenario: Successful add a todo
    Given Empty Todo list
    When User write "buy some milk" to ".test-input" and click to ".btn-success"
    Then User should see "buy some milk" item in todoList
Scenario: Fail adding empty todo
    Given Empty todo text
    When User write empty todo to ".test-input" and click to ".btn-success"
    Then User should not see new todo in todoList