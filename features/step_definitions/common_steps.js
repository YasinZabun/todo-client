const { Given, When, Then, Before, After} = require('@cucumber/cucumber');
const { expect } = require('chai')
var {setDefaultTimeout} = require('@cucumber/cucumber');
setDefaultTimeout(60 * 1000);
const puppeteer =require('puppeteer')
let browser
let page
let todoElementCount

Before(async function(){
    browser= await puppeteer.launch({ headless: true });
    page= await browser.newPage()
    await page.goto('http://52.156.77.163:8080/',{waitUntil: 'networkidle0', timeout: 100000})

})
After(async function(){
   await browser.close()
})
Given('Empty Todo list',()=>{
})
When('User write {string} to {string} and click to {string}',async function(todoText,inputElement,clickElement){
    await page.type(inputElement,todoText)
    await page.click(clickElement)
    await new Promise(r => setTimeout(r, 3000));
})
Then('User should see {string} item in todoList',async function(todoText){
    const allTodos = await page.evaluate(() => Array.from(document.getElementsByClassName('font-italic'), e => e.innerText));
    var firstAddedTodo=allTodos[0].split("\n")
    expect(firstAddedTodo[0]).to.equal(todoText)
})
Given('Empty todo text',()=>{
})
When('User write empty todo to {string} and click to {string}',async function(inputElement,clickElement){
    todoElementCount=await page.evaluate(() =>document.getElementsByClassName('font-italic').length);
    await page.type(inputElement,"")
    await page.click(clickElement)
    await new Promise(r => setTimeout(r, 3000));
})
Then('User should not see new todo in todoList',async function(){
    lastTodoElementCount=await page.evaluate(() =>document.getElementsByClassName('font-italic').length);
    expect(todoElementCount).to.equal(lastTodoElementCount)
})